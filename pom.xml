<?xml version="1.0" encoding="UTF-8"?>

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 https://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <parent>
    <groupId>nl.demon.shadowland.freedumbytes.maven.config</groupId>
    <artifactId>java</artifactId>
    <version>5.1.1</version>
  </parent>

  <groupId>nl.demon.shadowland.freedumbytes.documentation.docbook</groupId>
  <artifactId>docbook</artifactId>
  <version>1.0.0-SNAPSHOT</version>
  <packaging>pom</packaging>

  <name>${organizationName} DocBook Tools</name>
  <description>DocBook documentation tools setup.</description>
  <url>${projectRoot}</url>
  <inceptionYear>2012</inceptionYear>

  <licenses>
    <license>
      <name>Apache License, Version 2.0</name>
      <url>https://www.apache.org/licenses/LICENSE-2.0.html</url>
      <distribution>repo</distribution>
    </license>
  </licenses>

  <scm>
    <connection>${sourceConnection}</connection>
    <developerConnection>${sourceDevConnection}</developerConnection>
    <url>${sourceWebRoot}</url>
    <tag />
  </scm>

  <ciManagement>
    <system>${ciSystem}</system>
    <url>${ciWebRoot}</url>
  </ciManagement>

  <issueManagement>
    <system>${issueSystem}</system>
    <url>${issueWebRoot}</url>
  </issueManagement>

  <distributionManagement>
    <site>
      <id>mvn-sites</id>
      <name>Maven Documentation Sites</name>
      <url>${distributionManagementSiteUrlPrefix}${projectRoot}</url>
    </site>
  </distributionManagement>

  <modules>
    <module>custom-docbook-style</module>
    <module>custom-docbook-xslt</module>
    <module>custom-pressgang-highlight</module>
    <module>custom-jdocbook-core</module>
    <module>maven-docbook</module>
  </modules>

  <properties>
    <projectRoot>${mavenHost}/docbook</projectRoot>

    <sourceConnection>scm:git:${gitRepo}/docbook.git</sourceConnection>
    <sourceDevConnection>scm:git:${gitRepo}/docbook.git</sourceDevConnection>
    <sourceWebRoot>${fisheyeHost}/browse/docbook</sourceWebRoot>

    <ciSystem>Jenkins</ciSystem>
    <ciWebRoot>${jenkinsHost}/job/docbook</ciWebRoot>

    <issueSystem>JIRA</issueSystem>
    <issueWebRoot>${jiraHost}/projects/DCSDB</issueWebRoot>

    <maven.compiler.compilerVersion>1.8</maven.compiler.compilerVersion>
    <maven.compiler.source>${maven.compiler.target}</maven.compiler.source>
    <maven.compiler.target>${maven.compiler.compilerVersion}</maven.compiler.target>
    <maven.compiler.testSource>${maven.compiler.testTarget}</maven.compiler.testSource>
    <maven.compiler.testTarget>${maven.compiler.target}</maven.compiler.testTarget>

    <mojoSignatureArtifactId>java18</mojoSignatureArtifactId>
    <ignoreDependencies>true</ignoreDependencies>

    <extraEnforcerRulesMaxJdkVersion>${maven.compiler.target}</extraEnforcerRulesMaxJdkVersion>

    <javadocVersion>1.8.0</javadocVersion>
    <javadocDoclint>none</javadocDoclint>

    <aggregateSurefireReports>true</aggregateSurefireReports>
    <surefire.useSystemClassLoader>true</surefire.useSystemClassLoader>
    <failsafe.useSystemClassLoader>true</failsafe.useSystemClassLoader>

    <installAtEnd>false</installAtEnd>
    <deployAtEnd>false</deployAtEnd>

    <docbookXslVersion>1.78.1</docbookXslVersion>
    <fopVersion>2.6</fopVersion>
    <pressgangJdocbookCoreVersion>1.1.1</pressgangJdocbookCoreVersion>
    <patchJdocbookCoreVersion>${pressgangJdocbookCoreVersion}</patchJdocbookCoreVersion>
    <pressgangMavenJdocbookStylePluginVersion>2.0.0</pressgangMavenJdocbookStylePluginVersion>
    <pressgangMavenJdocbookPluginVersion>2.3.9</pressgangMavenJdocbookPluginVersion>
    <pressgangToolsVersion>3.1.4</pressgangToolsVersion>
    <patchPressgangToolsVersion>${pressgangToolsVersion}</patchPressgangToolsVersion>
  </properties>

  <profiles>
    <profile>
      <id>openSource</id>

      <properties>
        <projectRoot>${gitlabHost}/docbook</projectRoot>

        <sourceConnection>scm:git:${gitlabRepo}/docbook.git</sourceConnection>
        <sourceDevConnection>scm:git:${gitlabRepoSSH}/docbook.git</sourceDevConnection>
        <sourceWebRoot>${gitlabRepo}/docbook/tree/master</sourceWebRoot>

        <ciSystem>GitLab CI</ciSystem>
        <ciWebRoot>${gitlabRepo}/docbook/pipelines</ciWebRoot>

        <issueSystem>GitLab</issueSystem>
        <issueWebRoot>${gitlabRepo}/docbook/issues</issueWebRoot>
      </properties>
    </profile>

    <profile>
      <id>enableGitLabRepo</id>

      <activation>
        <property>
          <name>env.CI_PROJECT_ID</name>
        </property>
      </activation>

      <repositories>
        <repository>
          <id>gitlab-maven</id>
          <name>GitLab Group Maven Packages</name>
          <url>https://gitlab.com/api/v4/groups/freedumbytes/-/packages/maven</url>
        </repository>
      </repositories>

      <properties>
        <projectRoot>${gitlabHost}/docbook</projectRoot>

        <sourceConnection>scm:git:${gitlabRepo}/docbook.git</sourceConnection>
        <sourceDevConnection>scm:git:${gitlabRepoSSH}/docbook.git</sourceDevConnection>
        <sourceWebRoot>${gitlabRepo}/docbook/tree/master</sourceWebRoot>

        <ciSystem>GitLab CI</ciSystem>
        <ciWebRoot>${gitlabRepo}/docbook/pipelines</ciWebRoot>

        <issueSystem>GitLab</issueSystem>
        <issueWebRoot>${gitlabRepo}/docbook/issues</issueWebRoot>
      </properties>
    </profile>

    <profile>
      <id>jbossRepo</id>

      <activation>
        <property>
          <name>!nexusRepo</name>
        </property>
      </activation>

      <repositories>
        <repository>
          <id>central</id>
          <name>Central Repository</name>
          <url>https://repo.maven.apache.org/maven2</url>
          <layout>default</layout>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
        </repository>

        <repository>
          <id>jboss</id>
          <name>JBoss Releases</name>
          <url>https://repository.jboss.org/nexus/content/repositories/releases</url>
          <layout>default</layout>
          <releases>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
          </releases>
          <snapshots>
            <enabled>false</enabled>
            <updatePolicy>always</updatePolicy>
          </snapshots>
        </repository>

        <repository>
          <id>jboss-thirdparty</id>
          <name>JBoss Thirdparty Releases</name>
          <url>https://repository.jboss.org/nexus/content/repositories/thirdparty-releases</url>
          <layout>default</layout>
          <releases>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
          </releases>
          <snapshots>
            <enabled>false</enabled>
            <updatePolicy>always</updatePolicy>
          </snapshots>
        </repository>
      </repositories>
    </profile>
  </profiles>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>org.jboss.pressgang</groupId>
        <artifactId>pressgang-highlight</artifactId>
        <version>${pressgangToolsVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.apache.xmlgraphics</groupId>
        <artifactId>fop</artifactId>
        <version>${fopVersion}</version>
        <exclusions>
          <exclusion>
            <groupId>org.apache.avalon.framework</groupId>
            <artifactId>avalon-framework-api</artifactId>
          </exclusion>
          <exclusion>
            <groupId>org.apache.avalon.framework</groupId>
            <artifactId>avalon-framework-impl</artifactId>
          </exclusion>
        </exclusions>
      </dependency>

      <dependency>
        <groupId>org.apache.xmlgraphics</groupId>
        <artifactId>xmlgraphics-commons</artifactId>
        <version>${fopVersion}</version>
      </dependency>

      <dependency>
        <groupId>org.jboss.jdocbook</groupId>
        <artifactId>jdocbook-core</artifactId>
        <version>${pressgangJdocbookCoreVersion}</version>
        <exclusions>
          <exclusion>
            <groupId>net.sf.docbook</groupId>
            <artifactId>docbook-xml</artifactId>
          </exclusion>
          <exclusion>
            <groupId>net.sf.docbook</groupId>
            <artifactId>docbook-xsl</artifactId>
          </exclusion>
        </exclusions>
      </dependency>
    </dependencies>
  </dependencyManagement>
</project>
