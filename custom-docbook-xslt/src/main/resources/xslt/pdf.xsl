<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">
  <xsl:import href="http://docbook.sourceforge.net/release/xsl/1.78.1/fo/docbook.xsl" />

  <xsl:include href="common/en.xml" />

  <xsl:include href="fo/param.xsl" />

  <xsl:include href="fo/autotoc.xsl" />
  <xsl:include href="fo/callout.xsl" />
  <xsl:include href="fo/font.xsl" />
  <xsl:include href="fo/graphics.xsl" />
  <xsl:include href="fo/inline.xsl" />
  <xsl:include href="fo/programlisting.xsl" />
  <xsl:include href="fo/pagesetup.xsl" />
  <xsl:include href="fo/titlepage.templates.xsl" />
  <xsl:include href="fo/titlepage.xsl" />
</xsl:stylesheet>
