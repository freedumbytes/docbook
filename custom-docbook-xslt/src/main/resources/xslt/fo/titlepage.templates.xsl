<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">

  <xsl:template name="book.titlepage.recto">
    <xsl:choose>
      <xsl:when test="d:bookinfo/d:title">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:title" />
      </xsl:when>
      <xsl:when test="d:info/d:title">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:title" />
      </xsl:when>
      <xsl:when test="d:title">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:title" />
      </xsl:when>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="d:bookinfo/d:subtitle">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:subtitle" />
      </xsl:when>
      <xsl:when test="d:info/d:subtitle">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:subtitle" />
      </xsl:when>
      <xsl:when test="d:subtitle">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:subtitle" />
      </xsl:when>
    </xsl:choose>

    <xsl:if test="($confidential.mode = 'yes' or ($confidential.mode = 'maybe' and ancestor-or-self::*[@security][1]/@security = 'confidential'))">
      <xsl:choose>
        <xsl:when test="$confidential.filename != ''">
          <fo:block text-align="center">
            <fo:external-graphic width="auto" height="auto" content-width="{$confidential.image.width}">
              <xsl:attribute name="src">
                <xsl:call-template name="confidential.graphic" />
              </xsl:attribute>
            </fo:external-graphic>
          </fo:block>
        </xsl:when>
        <xsl:otherwise>
          <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format" xsl:use-attribute-sets="book.titlepage.recto.style" text-align="center" font-size="24pt" space-before="18.6624pt" font-weight="bold" font-family="{$title.fontset}" color="crimson">
            <xsl:call-template name="string.upper">
              <xsl:with-param name="string" select="$confidential.text" />
            </xsl:call-template>
          </fo:block>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>

    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:cover" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:corpauthor" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:corpauthor" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:org" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:org" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:authorgroup" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:authorgroup" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:author" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:author" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:othercredit" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:othercredit" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:collab" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:collab" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:copyright" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:copyright" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:publisher" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:publisher" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:pubdate" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:pubdate" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:edition" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:edition" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:editor" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:editor" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:legalnotice" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:legalnotice" />
  </xsl:template>

  <xsl:template name="book.titlepage.verso" />

  <xsl:template name="book.titlepage.separator" />
  <xsl:template name="book.titlepage.before.recto" />
  <xsl:template name="book.titlepage.before.verso" />

  <xsl:template name="book.titlepage">
    <fo:block xmlns:fo="http://www.w3.org/1999/XSL/Format">
      <xsl:call-template name="book.titlepage.before.recto" />
      <fo:block>
        <xsl:call-template name="book.titlepage.recto" />
      </fo:block>
      <xsl:call-template name="book.titlepage.separator" />
      <fo:block>
        <xsl:call-template name="book.titlepage.verso" />
      </fo:block>
      <xsl:call-template name="book.titlepage.separator" />
    </fo:block>
  </xsl:template>

  <xsl:template name="component.title.nomarkup">
    <xsl:param name="node" select="." />

    <xsl:variable name="id">
      <xsl:call-template name="object.id">
        <xsl:with-param name="object" select="$node" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:variable name="title">
      <xsl:apply-templates select="$node" mode="object.title.markup">
        <xsl:with-param name="allow-anchors" select="1" />
      </xsl:apply-templates>
    </xsl:variable>
    <xsl:copy-of select="$title" />
  </xsl:template>

  <xsl:template name="confidential.graphic">
    <xsl:variable name="filename">
      <xsl:value-of select="$confidential.graphics.path"/>
      <xsl:value-of select="$confidential.filename" />
    </xsl:variable>

    <xsl:choose>
      <xsl:when test="$fop.extensions != 0 or $arbortext.extensions != 0">
        <xsl:value-of select="$filename" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>url(</xsl:text>
        <xsl:value-of select="$filename" />
        <xsl:text>)</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
