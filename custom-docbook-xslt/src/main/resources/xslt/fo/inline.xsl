<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">
  <xsl:template match="d:application">
    <xsl:call-template name="inline.boldseq" />
  </xsl:template>

  <xsl:template match="d:command">
    <xsl:call-template name="inline.monoseq" />
  </xsl:template>

  <xsl:template match="d:guibutton">
    <xsl:call-template name="inline.boldseq" />
  </xsl:template>

  <xsl:template match="d:guiicon">
    <xsl:call-template name="inline.boldseq" />
  </xsl:template>

  <xsl:template match="d:guilabel">
    <xsl:call-template name="inline.boldseq" />
  </xsl:template>

  <xsl:template match="d:guimenu">
    <xsl:call-template name="inline.boldseq" />
  </xsl:template>

  <xsl:template match="d:guimenuitem">
    <xsl:call-template name="inline.boldseq" />
  </xsl:template>

  <xsl:template match="d:guisubmenu">
    <xsl:call-template name="inline.boldseq" />
  </xsl:template>

  <xsl:template match="d:accel">
    <fo:inline text-decoration="underline">
      <xsl:call-template name="inline.boldseq" />
    </fo:inline>
  </xsl:template>

  <xsl:template match="d:menuchoice">
    <xsl:variable name="shortcut" select="./shortcut"/>
    <xsl:call-template name="process.menuchoice"/>
    <xsl:if test="$shortcut">
      <xsl:text> (</xsl:text>
      <xsl:apply-templates select="$shortcut"/>
      <xsl:text>)</xsl:text>
    </xsl:if>
  </xsl:template>

  <xsl:template name="process.menuchoice">
    <xsl:param name="nodelist" select="d:guibutton|d:guiicon|d:guilabel|d:guimenu|d:guimenuitem|d:guisubmenu|d:interface" /><!-- not(shortcut) -->
    <xsl:param name="count" select="1" />

    <xsl:choose>
      <xsl:when test="$count>count($nodelist)"></xsl:when>
      <xsl:when test="$count=1">
        <xsl:apply-templates select="$nodelist[$count=position()]" />
        <xsl:call-template name="process.menuchoice">
          <xsl:with-param name="nodelist" select="$nodelist" />
          <xsl:with-param name="count" select="$count+1" />
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:variable name="node" select="$nodelist[$count=position()]" />
        <xsl:choose>
          <xsl:when test="local-name($node)='guimenuitem' or local-name($node)='guisubmenu'">
            <xsl:copy-of select="$menuchoice.menu.separator" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="$menuchoice.separator" />
          </xsl:otherwise>
        </xsl:choose>
        <xsl:apply-templates select="$node" />
        <xsl:call-template name="process.menuchoice">
          <xsl:with-param name="nodelist" select="$nodelist" />
          <xsl:with-param name="count" select="$count+1" />
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
