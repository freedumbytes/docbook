<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">
  <xsl:template name="toc.line">
    <xsl:variable name="id">
      <xsl:call-template name="object.id" />
    </xsl:variable>

    <xsl:variable name="label">
      <xsl:apply-templates select="." mode="label.markup" />
    </xsl:variable>

    <fo:block text-align-last="justify"
              end-indent="{$toc.indent.width}pt"
              last-line-end-indent="-{$toc.indent.width}pt">
      <fo:inline keep-with-next.within-line="always">
        <fo:basic-link internal-destination="{$id}">

          <!-- Chapter titles should be bold. -->
          <xsl:choose>
            <xsl:when test="local-name(.) = 'chapter'">
              <xsl:attribute name="font-weight">bold</xsl:attribute>
            </xsl:when>
            <xsl:when test="local-name(.) = 'appendix'">
              <xsl:attribute name="font-weight">bold</xsl:attribute>
            </xsl:when>
          </xsl:choose>

          <xsl:if test="$label != ''">
            <xsl:copy-of select="$label" />
            <xsl:value-of select="$autotoc.label.separator" />
          </xsl:if>
          <xsl:apply-templates select="." mode="titleabbrev.markup" />
        </fo:basic-link>
      </fo:inline>
      <fo:inline keep-together.within-line="always">
        <xsl:text> </xsl:text>
        <fo:leader leader-pattern="dots"
                   leader-pattern-width="3pt"
                   leader-alignment="reference-area"
                   keep-with-next.within-line="always" />
        <xsl:text> </xsl:text>
        <fo:basic-link internal-destination="{$id}">
          <fo:page-number-citation ref-id="{$id}" />
        </fo:basic-link>
      </fo:inline>
    </fo:block>
  </xsl:template>
</xsl:stylesheet>
