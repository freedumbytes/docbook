<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">
  <xsl:template name="callout-bug">
    <xsl:param name="conum" select='1' />

    <xsl:choose>
      <!-- Draw callouts as images -->
      <xsl:when test="$callout.graphics != '0' and $conum &lt;= $callout.graphics.number.limit">
        <xsl:variable name="filename" select="concat($callout.graphics.path, $conum, $callout.graphics.extension)" />

        <fo:external-graphic content-width="{$callout.icon.size}" width="{$callout.icon.size}" padding="0.0em 0.1em 0.0em 0.1em" margin="0.0em" baseline-shift="-0.250em">
          <xsl:attribute name="src">
            <xsl:choose>
              <xsl:when test="$passivetex.extensions != 0 or $fop.extensions != 0 or $arbortext.extensions != 0">
                <xsl:value-of select="$filename" />
              </xsl:when>
              <xsl:otherwise>
                <xsl:text>url(</xsl:text>
                <xsl:value-of select="$filename" />
                <xsl:text>)</xsl:text>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:attribute>
        </fo:external-graphic>
      </xsl:when>

      <xsl:when test="$callout.unicode != 0 and $conum &lt;= $callout.unicode.number.limit">
        <xsl:variable name="comarkup">
          <xsl:choose>
            <xsl:when test="$callout.unicode.start.character = 10102">
              <xsl:choose>
                <xsl:when test="$conum = 1">&#10102;</xsl:when>
                <xsl:when test="$conum = 2">&#10103;</xsl:when>
                <xsl:when test="$conum = 3">&#10104;</xsl:when>
                <xsl:when test="$conum = 4">&#10105;</xsl:when>
                <xsl:when test="$conum = 5">&#10106;</xsl:when>
                <xsl:when test="$conum = 6">&#10107;</xsl:when>
                <xsl:when test="$conum = 7">&#10108;</xsl:when>
                <xsl:when test="$conum = 8">&#10109;</xsl:when>
                <xsl:when test="$conum = 9">&#10110;</xsl:when>
                <xsl:when test="$conum = 10">&#10111;</xsl:when>
              </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
              <xsl:message>
                <xsl:text>Don't know how to generate Unicode callouts </xsl:text>
                <xsl:text>when $callout.unicode.start.character is </xsl:text>
                <xsl:value-of select="$callout.unicode.start.character" />
              </xsl:message>
              <fo:inline background-color="#404040" color="white" padding-top="0.1em" padding-bottom="0.1em" padding-start="0.2em" padding-end="0.2em" baseline-shift="0.1em" font-family="{$body.fontset}" font-weight="bold" font-size="75%">
                <xsl:value-of select="$conum" />
              </fo:inline>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:choose>
          <xsl:when test="$callout.unicode.font != ''">
            <fo:inline font-family="{$callout.unicode.font}">
              <xsl:copy-of select="$comarkup" />
            </fo:inline>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy-of select="$comarkup" />
          </xsl:otherwise>
        </xsl:choose>
      </xsl:when>

      <!-- Most safe: draw a dark gray square with a white number inside -->
      <xsl:otherwise>
        <fo:inline background-color="#404040" color="white" padding-top="0.1em" padding-bottom="0.1em" padding-start="0.2em" padding-end="0.2em" baseline-shift="0.1em" font-family="{$body.fontset}" font-weight="bold" font-size="75%">
          <xsl:value-of select="$conum" />
        </fo:inline>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
