<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">
  <xsl:template name="pickfont">
    <xsl:variable name="font">
      <xsl:choose>
        <xsl:when test="$l10n.gentext.language = 'ja-JP' or l10n.gentext.language = 'ja'">
          <xsl:text>Sazanami Gothic,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'ko-KR' or $l10n.gentext.language = 'ko'">
          <xsl:text>Baekmuk Batang,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'zh-CN'">
          <xsl:text>ZYSong18030,AR PL UMing CN,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'as-IN' or $l10n.gentext.language = 'as'">
          <xsl:text>Lohit Bengali,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'bn-IN' or $l10n.gentext.language = 'bn'">
          <xsl:text>Lohit Bengali,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'ta-IN' or $l10n.gentext.language = 'ta'">
          <xsl:text>Lohit Tamil,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'pa-IN' or l10n.gentext.language = 'pa'">
          <xsl:text>Lohit Punjabi,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'hi-IN' or $l10n.gentext.language = 'hi'">
          <xsl:text>Lohit Hindi,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'mr-IN' or $l10n.gentext.language = 'mr'">
          <xsl:text>Lohit Hindi,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'gu-IN' or $l10n.gentext.language = 'gu'">
          <xsl:text>Lohit Gujarati,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'zh-TW'">
          <xsl:text>AR PL ShanHeiSun Uni,AR PL UMing TW,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'kn-IN' or $l10n.gentext.language = 'kn'">
          <xsl:text>Lohit Kannada,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'ml-IN' or $l10n.gentext.language = 'ml-IN'">
          <xsl:text>Lohit Malayalam,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'or-IN' or $l10n.gentext.language = 'or'">
          <xsl:text>Lohit Oriya,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'te-IN' or $l10n.gentext.language = 'te'">
          <xsl:text>Lohit Telugu,</xsl:text>
        </xsl:when>
        <xsl:when test="$l10n.gentext.language = 'si-LK' or $l10n.gentext.language = 'si'">
          <xsl:text>LKLUG,</xsl:text>
        </xsl:when>
      </xsl:choose>
    </xsl:variable>

    <xsl:message>
      <xsl:text>pickfont selection [lang=</xsl:text>
      <xsl:value-of select="$l10n.gentext.language" />
      <xsl:text>] : </xsl:text>
      <xsl:copy-of select="$font" />
    </xsl:message>

    <xsl:copy-of select="$font" />
  </xsl:template>

  <xsl:template name="pickfont-mono">
    <xsl:text>
      Liberation Mono,
      Lucida Console,
      Monaco,
      Consolas,
      Courier New,
      Courier,
      Arial Unicode MS,
      Lucida Sans Unicode,
      monospace
    </xsl:text>
  </xsl:template>

  <xsl:template name="pickfont-sans">
    <xsl:text>
      Ubuntu,
      Euphemia,
      SimSun,
      DokChampa,
      Batang,
      Gulim,
      Microsoft Himalaya,
      Dzongkha,
      MV Boli,
      Gautami,
      Vani,
      Iskoola Pota,
      Kalinga,
      Kartika,
      Latha,
      Leelawadee,
      Mangal,
      MoolBoran,
      Nyala,
      Raavi,
      Shruti,
      Sylfaen,
      Tunga,
      Vijaya,
      Vrinda,
      Microsoft Yi Baiti,
      Meiryo,
      Lucida Sans Unicode,
      Georgia,
      Verdana,
      Arial Unicode MS,
      Arial,
      Geneva,
      Helvetica,
      Noto Sans,
      Noto Sans Canadian Aboriginal,
      Noto Kufi Arabic,
      Noto Sans Hebrew,
      Noto Sans CJK KR,
      Noto Sans CJK JP,
      Noto Sans CJK TC,
      Noto Sans CJK SC,
      Noto Sans Lao,
      Noto Sans Armenian,
      Noto Sans Bengali,
      Noto Sans Devanagari,
      Noto Sans Ethiopic,
      Noto Sans Georgian,
      Noto Sans Gujarati,
      Noto Sans Gurmukhi,
      Noto Sans Kannada,
      Noto Sans Khmer,
      Noto Sans Malayalam,
      Noto Sans Oriya,
      Noto Sans Sinhala,
      Noto Sans Tai Viet,
      Noto Sans Tamil,
      Noto Sans Telugu,
      Noto Sans Thaana,
      Noto Sans Thai,
      Noto Sans Tibetan,
      Noto Sans Yi,
      Noto Sans Javanese,
      Javanese Text,
      Noto Sans Grantha,
      Noto Sans Myanmar,
      Myanmar Text,
      Noto Sans Sundanese,
      Noto Sans Tagalog,
      sans-serif
    </xsl:text>
  </xsl:template>

  <xsl:template name="pickfont-serif">
    <xsl:text>
      Ubuntu,
      Meiryo,
      Lucida Sans Unicode,
      Georgia,
      Arial Unicode MS,
      Arial,
      Noto Serif,
      Noto Naskh Arabic,
      Noto Serif Lao,
      Noto Serif Armenian,
      Noto Serif Georgian,
      Noto Serif Khmer,
      Noto Serif Thai,
      serif
    </xsl:text>
  </xsl:template>

  <xsl:template name="pickfont-dingbat">
    <xsl:call-template name="pickfont-sans"/>
  </xsl:template>

  <xsl:template name="pickfont-symbol">
    <xsl:text>
      Symbol,
      ZapfDingbats
    </xsl:text>
  </xsl:template>
</xsl:stylesheet>
