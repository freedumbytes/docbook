<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">
  <xsl:param name="admon.graphics" select="1" />
  <xsl:param name="admon.graphics.extension" select="'.png'" />
  <xsl:param name="admon.graphics.path">
    <xsl:if test="$img.src.path != ''">
      <xsl:value-of select="$img.src.path" />
    </xsl:if>
    <xsl:text>images/community/docbook/admonitions/</xsl:text>
  </xsl:param>
  <xsl:attribute-set name="admonition.title.properties">
    <xsl:attribute name="font-size">13pt</xsl:attribute>
    <xsl:attribute name="color">
      <xsl:choose>
        <xsl:when test="self::d:note">#4C5253</xsl:when>
        <xsl:when test="self::d:caution">#533500</xsl:when>
        <xsl:when test="self::d:important">white</xsl:when>
        <xsl:when test="self::d:warning">white</xsl:when>
        <xsl:when test="self::d:tip">white</xsl:when>
        <xsl:otherwise>white</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
  </xsl:attribute-set>

  <xsl:param name="body.font.family">
    <xsl:call-template name="pickfont-sans" />
  </xsl:param>
  <xsl:param name="body.margin.bottom">10mm</xsl:param>
  <xsl:param name="body.margin.top">10mm</xsl:param>
  <xsl:param name="body.start.indent">0pt</xsl:param>
  <xsl:param name="body.font.master">8</xsl:param>

  <xsl:param name="callouts.extension">1</xsl:param>
  <xsl:param name="callout.defaultcolumn">80</xsl:param>
  <xsl:param name="callout.graphics.extension">.png</xsl:param>
  <xsl:param name="callout.graphics" select="1"/>
  <xsl:param name="callout.icon.size">10pt</xsl:param>
  <xsl:param name="callout.graphics.number.limit">15</xsl:param>
  <xsl:param name="callout.graphics.path">
    <xsl:if test="$img.src.path != ''">
      <xsl:value-of select="$img.src.path"/>
    </xsl:if>
    <xsl:text>images/community/docbook/callouts/</xsl:text>
  </xsl:param>

  <xsl:param name="chapter.title.color" select="$title.color" />

  <xsl:attribute-set name="component.title.properties">
    <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
    <xsl:attribute name="space-before.optimum">
      <xsl:value-of select="concat($body.font.master, 'pt')" />
    </xsl:attribute>
    <xsl:attribute name="space-before.minimum">
      <xsl:value-of select="concat($body.font.master, 'pt')" />
    </xsl:attribute>
    <xsl:attribute name="space-before.maximum">
      <xsl:value-of select="concat($body.font.master, 'pt')" />
    </xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="color">
      <xsl:choose>
        <xsl:when test="not(parent::d:chapter | parent::d:article | parent::d:appendix)">
          <xsl:value-of select="$title.color" />
        </xsl:when>
        <xsl:otherwise><xsl:value-of select="$chapter.title.color" /></xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="text-align">
      <xsl:choose>
        <xsl:when test="((parent::d:article | parent::d:articleinfo) and not(ancestor::d:book) and not(self::d:bibliography)) or (parent::d:slides | parent::d:slidesinfo)">center</xsl:when>
        <xsl:otherwise>left</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="start-indent">
      <xsl:value-of select="$title.margin.left" />
    </xsl:attribute>
  </xsl:attribute-set>

  <xsl:param name="confidential.mode">maybe</xsl:param>
  <xsl:param name="confidential.text">Confidential</xsl:param>
  <xsl:param name="confidential.graphics.path">
    <xsl:if test="$img.src.path != ''">
      <xsl:value-of select="$img.src.path" />
    </xsl:if>
    <xsl:text>images/community/docbook/status/</xsl:text>
  </xsl:param>
  <xsl:param name="confidential.filename">confidential.png</xsl:param>
  <xsl:param name="confidential.image.width">243pt</xsl:param>

  <xsl:param name="double.sided" select="1" />

  <xsl:param name="draft.mode">maybe</xsl:param>
  <xsl:param name="draft.watermark.image">http://docbook.sourceforge.net/release/images/draft.png</xsl:param>

  <xsl:param name="editedby.enabled" select="1" />
  <xsl:param name="publishedby.enabled" select="1" />
  <xsl:param name="blurb.on.titlepage.enabled" select="1" />

  <xsl:param name="fop.extensions" select="0" />
  <xsl:param name="fop1.extensions" select="1" />

  <!-- Make examples, tables etc. break across pages -->
  <xsl:attribute-set name="formal.object.properties">
    <xsl:attribute name="keep-together.within-column">auto</xsl:attribute>
  </xsl:attribute-set>

  <xsl:param name="formal.title.placement">
    figure before
    example before
    equation before
    table before
    procedure before
  </xsl:param>

  <xsl:param name="footnote.number.format" select="'1'" />
  <xsl:param name="footnote.number.symbols" select="''" />
  <xsl:attribute-set name="footnote.properties">
    <xsl:attribute name="padding-top">48pt</xsl:attribute>
    <xsl:attribute name="font-family">
      <xsl:value-of select="$body.fontset" />
    </xsl:attribute>
    <xsl:attribute name="font-size">
      <xsl:value-of select="$footnote.font.size" />
    </xsl:attribute>
    <xsl:attribute name="font-weight">normal</xsl:attribute>
    <xsl:attribute name="font-style">normal</xsl:attribute>
    <xsl:attribute name="text-align">
      <xsl:value-of select="$alignment" />
    </xsl:attribute>
    <xsl:attribute name="start-indent">0pt</xsl:attribute>
  </xsl:attribute-set>

  <xsl:param name="generate.toc">
    set toc
    book toc, title
    article nop
  </xsl:param>

  <xsl:attribute-set name="graphical.admonition.properties">
    <xsl:attribute name="color">
      <xsl:choose>
        <xsl:when test="self::d:note">#4C5253</xsl:when>
        <xsl:when test="self::d:caution">#533500</xsl:when>
        <xsl:when test="self::d:important">white</xsl:when>
        <xsl:when test="self::d:warning">white</xsl:when>
        <xsl:when test="self::d:tip">white</xsl:when>
        <xsl:otherwise>white</xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="background-color">
      <xsl:choose>
        <xsl:when test="self::d:note">#B5BCBD</xsl:when>
        <xsl:when test="self::d:caution">#E3A835</xsl:when>
        <xsl:when test="self::d:important">#4A5D75</xsl:when>
        <xsl:when test="self::d:warning">#7B1E1E</xsl:when>
        <xsl:when test="self::d:tip">#7E917F</xsl:when>
        <xsl:otherwise>#404040</xsl:otherwise>
        </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
    <xsl:attribute name="space-before.minimum">0.8em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
    <xsl:attribute name="space-after.optimum">1em</xsl:attribute>
    <xsl:attribute name="space-after.minimum">0.8em</xsl:attribute>
    <xsl:attribute name="space-after.maximum">1em</xsl:attribute>
    <xsl:attribute name="padding-bottom">12pt</xsl:attribute>
    <xsl:attribute name="padding-top">12pt</xsl:attribute>
    <xsl:attribute name="padding-right">12pt</xsl:attribute>
    <xsl:attribute name="padding-left">12pt</xsl:attribute>
    <xsl:attribute name="margin-left">
      <xsl:value-of select="$title.margin.left" />
    </xsl:attribute>
  </xsl:attribute-set>

  <!-- scalefit for large images -->
  <xsl:param name="graphicsize.extension" select="'1'" />
  <xsl:param name="default.image.width">16.0cm</xsl:param>
  <xsl:param name="default.inline.image.height">1em</xsl:param>

  <xsl:param name="header.column.widths">
    <xsl:choose>
      <xsl:when test="($confidential.mode = 'yes' or $confidential.mode = 'maybe')">
        <xsl:value-of select="'3 1 3'" />
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="'1 0 1'" />
      </xsl:otherwise>
    </xsl:choose>
  </xsl:param>

  <xsl:param name="l10n.gentext.default.language">en</xsl:param>

  <!-- Enable line numbering extension. I have personally not gotten this to work yet -->
  <xsl:param name="linenumbering.extension">1</xsl:param>
  <xsl:param name="linenumbering.width">2</xsl:param>
  <xsl:param name="linenumbering.everyNth">1</xsl:param>

  <xsl:attribute-set name="list.item.spacing">
    <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
    <xsl:attribute name="space-before.minimum">0.8em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
    <xsl:attribute name="margin-left">0.5em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="compact.list.item.spacing">
    <xsl:attribute name="space-before.optimum">0em</xsl:attribute>
    <xsl:attribute name="space-before.minimum">0em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">0.2em</xsl:attribute>
    <xsl:attribute name="margin-left">0.5em</xsl:attribute>
  </xsl:attribute-set>

  <xsl:param name="menuchoice.menu.separator"> &#x2192; </xsl:param>
  <xsl:param name="menuchoice.separator"> &#x2192; </xsl:param>

  <xsl:param name="monospace.font.family">
    <xsl:call-template name="pickfont-mono" />
  </xsl:param>
  <xsl:attribute-set name="monospace.properties">
    <xsl:attribute name="font-size">8pt</xsl:attribute>
    <xsl:attribute name="font-family">
      <xsl:value-of select="$monospace.font.family" />
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="monospace.verbatim.properties"
                     use-attribute-sets="verbatim.properties monospace.properties">
    <xsl:attribute name="text-align">start</xsl:attribute>
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    <xsl:attribute name="hyphenation-character">-</xsl:attribute>
  </xsl:attribute-set>

  <xsl:param name="page.margin.bottom">15mm</xsl:param>
  <xsl:param name="page.margin.inner">30mm</xsl:param>
  <xsl:param name="page.margin.outer">20mm</xsl:param>
  <xsl:param name="page.margin.top">10mm</xsl:param>

  <xsl:param name="paper.type" select="'A4'" />

  <xsl:param name="programlisting.font" select="$monospace.font.family" />
  <xsl:param name="programlisting.font.size" select="'75%'" />

  <xsl:param name="qandadiv.autolabel" select="0" />

  <xsl:param name="region.after.extent">9mm</xsl:param>
  <xsl:param name="region.before.extent">9mm</xsl:param>

  <xsl:param name="sans.font.family">
    <xsl:call-template name="pickfont-sans" />
  </xsl:param>

  <xsl:param name="section.autolabel" select="1" />
  <xsl:param name="section.label.includes.component.label" select="1" />
  <xsl:param name="section.title.color" select="$title.color" />
  <xsl:attribute-set name="section.title.level1.properties">
    <xsl:attribute name="color">
      <xsl:value-of select="$section.title.color" />
    </xsl:attribute>
    <xsl:attribute name="font-size">
      <xsl:value-of select="$body.font.master * 1.6" />
      <xsl:text>pt</xsl:text>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="section.title.level2.properties">
    <xsl:attribute name="color">
      <xsl:value-of select="$section.title.color" />
    </xsl:attribute>
    <xsl:attribute name="font-size">
      <xsl:value-of select="$body.font.master * 1.4" />
      <xsl:text>pt</xsl:text>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="section.title.level3.properties">
    <xsl:attribute name="color">
      <xsl:value-of select="$section.title.color" />
    </xsl:attribute>
    <xsl:attribute name="font-size">
      <xsl:value-of select="$body.font.master * 1.3" />
      <xsl:text>pt</xsl:text>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="section.title.level4.properties">
    <xsl:attribute name="color">
      <xsl:value-of select="$section.title.color" />
    </xsl:attribute>
    <xsl:attribute name="font-size">
      <xsl:value-of select="$body.font.master * 1.2" />
      <xsl:text>pt</xsl:text>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="section.title.level5.properties">
    <xsl:attribute name="color">
      <xsl:value-of select="$section.title.color" />
    </xsl:attribute>
    <xsl:attribute name="font-size">
      <xsl:value-of select="$body.font.master * 1.1" />
      <xsl:text>pt</xsl:text>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="section.title.level6.properties">
    <xsl:attribute name="color">
      <xsl:value-of select="$section.title.color" />
    </xsl:attribute>
    <xsl:attribute name="font-size">
      <xsl:value-of select="$body.font.master" />
      <xsl:text>pt</xsl:text>
    </xsl:attribute>
  </xsl:attribute-set>
  <xsl:attribute-set name="section.title.properties">
    <xsl:attribute name="font-family">
      <xsl:value-of select="$title.font.family" />
    </xsl:attribute>
    <xsl:attribute name="font-weight">bold</xsl:attribute>
    <!-- font size is calculated dynamically by section.heading template -->
    <xsl:attribute name="keep-with-next.within-column">always</xsl:attribute>
    <xsl:attribute name="space-before.minimum">0.8em</xsl:attribute>
    <xsl:attribute name="space-before.optimum">1.0em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
    <xsl:attribute name="text-align">left</xsl:attribute>
    <xsl:attribute name="start-indent">
      <xsl:value-of select="$title.margin.left" />
    </xsl:attribute>
  </xsl:attribute-set>

  <xsl:param name="shade.verbatim" select="1" />
  <xsl:attribute-set name="shade.verbatim.style">
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    <xsl:attribute name="background-color">
      <xsl:choose>
        <xsl:when test="ancestor::d:note">
          <xsl:text>#D6DEE0</xsl:text>
        </xsl:when>
        <xsl:when test="ancestor::d:caution">
          <xsl:text>#FAF8ED</xsl:text>
        </xsl:when>
        <xsl:when test="ancestor::d:important">
          <xsl:text>#E1EEF4</xsl:text>
        </xsl:when>
        <xsl:when test="ancestor::d:warning">
          <xsl:text>#FAF8ED</xsl:text>
        </xsl:when>
        <xsl:when test="ancestor::d:tip">
          <xsl:text>#D5E1D5</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>#EDE7C8</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="color">
      <xsl:choose>
        <xsl:when test="ancestor::d:note">
          <xsl:text>#334558</xsl:text>
        </xsl:when>
        <xsl:when test="ancestor::d:caution">
          <xsl:text>#334558</xsl:text>
        </xsl:when>
        <xsl:when test="ancestor::d:important">
          <xsl:text>#334558</xsl:text>
        </xsl:when>
        <xsl:when test="ancestor::d:warning">
          <xsl:text>#334558</xsl:text>
        </xsl:when>
        <xsl:when test="ancestor::d:tip">
          <xsl:text>#334558</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>#333</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
    <xsl:attribute name="padding-left">12pt</xsl:attribute>
    <xsl:attribute name="padding-right">12pt</xsl:attribute>
    <xsl:attribute name="padding-top">6pt</xsl:attribute>
    <xsl:attribute name="padding-bottom">6pt</xsl:attribute>
    <xsl:attribute name="margin-left">
      <xsl:value-of select="$title.margin.left" />
    </xsl:attribute>
  </xsl:attribute-set>

  <!-- Only hairlines as frame and cell borders in tables -->
  <xsl:param name="table.cell.border.color">#5c5c4f</xsl:param>
  <xsl:param name="table.cell.border.left.color">white</xsl:param>
  <xsl:param name="table.cell.border.right.color">white</xsl:param>
  <xsl:param name="table.cell.border.thickness">0.15pt</xsl:param>
  <!-- Some padding inside tables -->
  <xsl:attribute-set name="table.cell.padding">
    <xsl:attribute name="padding-left">4pt</xsl:attribute>
    <xsl:attribute name="padding-right">4pt</xsl:attribute>
    <xsl:attribute name="padding-top">2pt</xsl:attribute>
    <xsl:attribute name="padding-bottom">2pt</xsl:attribute>
  </xsl:attribute-set>
  <xsl:param name="table.frame.border.color">#5c5c4f</xsl:param>
  <xsl:param name="table.frame.border.left.color">white</xsl:param>
  <xsl:param name="table.frame.border.right.color">white</xsl:param>
  <xsl:param name="table.frame.border.thickness">0.3pt</xsl:param>

  <xsl:param name="title.color">#000F55</xsl:param>
  <xsl:param name="title.font.family">
    <xsl:call-template name="pickfont-sans" />
  </xsl:param>

  <xsl:param name="titlepage.color" select="$title.color" />

  <xsl:param name="ulink.show" select="0"/>

  <xsl:param name="use.extensions">1</xsl:param>

  <!-- Format Variable Lists as Blocks (prevents horizontal overflow). -->
  <xsl:param name="variablelist.as.blocks">1</xsl:param>

  <xsl:attribute-set name="verbatim.properties">
    <xsl:attribute name="space-before.minimum">0.8em</xsl:attribute>
    <xsl:attribute name="space-before.optimum">1em</xsl:attribute>
    <xsl:attribute name="space-before.maximum">1.2em</xsl:attribute>
    <xsl:attribute name="space-after.minimum">0.8em</xsl:attribute>
    <xsl:attribute name="space-after.optimum">1em</xsl:attribute>
    <xsl:attribute name="space-after.maximum">1.2em</xsl:attribute>
    <xsl:attribute name="hyphenate">false</xsl:attribute>
    <xsl:attribute name="wrap-option">wrap</xsl:attribute>
    <xsl:attribute name="white-space-collapse">false</xsl:attribute>
    <xsl:attribute name="white-space-treatment">preserve</xsl:attribute>
    <xsl:attribute name="linefeed-treatment">preserve</xsl:attribute>
    <xsl:attribute name="text-align">start</xsl:attribute>
  </xsl:attribute-set>

  <xsl:attribute-set name="xref.properties">
    <xsl:attribute name="color">
      <xsl:choose>
        <xsl:when test="ancestor::d:note or ancestor::d:caution or ancestor::d:important or ancestor::d:warning or ancestor::d:tip">
          <xsl:text>#aee6ff</xsl:text>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>#0066cc</xsl:text>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:attribute>
  </xsl:attribute-set>
</xsl:stylesheet>
