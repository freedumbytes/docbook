<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">
  <xsl:template name="head.sep.rule">
    <xsl:param name="pageclass" />
    <xsl:param name="sequence" />
    <xsl:param name="gentext-key" />

    <xsl:if test="$header.rule != 0">
      <xsl:attribute name="border-bottom-width">0.5pt</xsl:attribute>
      <xsl:attribute name="border-bottom-style">solid</xsl:attribute>
      <xsl:attribute name="border-bottom-color">#000F55</xsl:attribute>
    </xsl:if>
  </xsl:template>

  <xsl:template name="foot.sep.rule">
    <xsl:param name="pageclass" />
    <xsl:param name="sequence" />
    <xsl:param name="gentext-key" />

    <xsl:if test="$footer.rule != 0">
      <xsl:attribute name="border-top-width">0.5pt</xsl:attribute>
      <xsl:attribute name="border-top-style">solid</xsl:attribute>
      <xsl:attribute name="border-top-color">#000F55</xsl:attribute>
    </xsl:if>
  </xsl:template>

  <xsl:template name="header.content">
    <xsl:param name="pageclass" select="''" />
    <xsl:param name="sequence" select="''" />
    <xsl:param name="position" select="''" />
    <xsl:param name="gentext-key" select="''" />
    <xsl:param name="title-limit" select="'60'" />
    <xsl:choose>
      <xsl:when test="($confidential.mode = 'yes' or ($confidential.mode = 'maybe' and ancestor-or-self::*[@security][1]/@security = 'confidential'))
                      and (($sequence='first' or $sequence='odd' or $sequence='even' or $sequence='blank') and $position='center')">
        <fo:inline keep-together.within-line="always" font-weight="bold" color="crimson">
          <xsl:value-of select="$confidential.text" />
        </fo:inline>
      </xsl:when>
      <xsl:when test="$sequence = 'blank'">
      </xsl:when>
      <xsl:when test="($sequence='first' and $position='left' and ($gentext-key='chapter' or $gentext-key='appendix'))">
        <xsl:variable name="text">
          <xsl:call-template name="component.title.nomarkup" />
        </xsl:variable>
        <xsl:variable name="chapt">
          <xsl:value-of select="substring-before($text, '&#xA0;')" />
        </xsl:variable>
        <xsl:variable name="remainder">
          <xsl:value-of select="substring-after($text, '&#xA0;')" />
        </xsl:variable>
        <xsl:variable name="chapt-num">
          <xsl:value-of select="substring-before($remainder, '&#xA0;')" />
        </xsl:variable>
        <xsl:variable name="text1">
          <xsl:value-of select="concat($chapt, '&#xA0;', $chapt-num)" />
        </xsl:variable>
        <fo:inline keep-together.within-line="always" font-weight="bold">
          <xsl:value-of select="$text1" />
        </fo:inline>
      </xsl:when>
      <xsl:when test="($sequence='even' and $position='left')">
        <xsl:variable name="text">
          <xsl:call-template name="component.title.nomarkup" />
        </xsl:variable>
        <xsl:variable name="chapt">
          <xsl:value-of select="substring-before($text, '&#xA0;')" />
        </xsl:variable>
        <xsl:variable name="remainder">
          <xsl:value-of select="substring-after($text, '&#xA0;')" />
        </xsl:variable>
        <xsl:variable name="chapt-num">
          <xsl:value-of select="substring-before($remainder, '&#xA0;')" />
        </xsl:variable>
        <xsl:variable name="text1">
          <xsl:value-of select="concat($chapt, '&#xA0;', $chapt-num)" />
        </xsl:variable>
        <fo:inline keep-together.within-line="always" font-weight="bold">
          <xsl:value-of select="$text1" />
        </fo:inline>
      </xsl:when>
      <xsl:when test="($sequence='odd' and $position='right')">
        <xsl:variable name="text">
          <xsl:call-template name="component.title.nomarkup" />
        </xsl:variable>
        <xsl:variable name="chapt">
          <xsl:value-of select="substring-before($text, '&#xA0;')" />
        </xsl:variable>
        <xsl:variable name="remainder">
          <xsl:value-of select="substring-after($text, '&#xA0;')" />
        </xsl:variable>
        <xsl:variable name="chapt-title">
          <xsl:value-of select="substring-after($remainder, '&#xA0;')" />
        </xsl:variable>
        <fo:inline keep-together.within-line="always" font-weight="bold">
          <xsl:choose>
            <xsl:when test="string-length($chapt-title) &gt; '$title-limit'">
              <xsl:value-of select="concat(substring($chapt-title, 0, $title-limit), '...')" />
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="$chapt-title" />
            </xsl:otherwise>
          </xsl:choose>
        </fo:inline>
      </xsl:when>
      <xsl:when test="$position='left'">
        <xsl:call-template name="draft.text" />
      </xsl:when>
      <xsl:when test="$position='center'">
      </xsl:when>
      <xsl:when test="$position='right'">
        <xsl:call-template name="draft.text" />
      </xsl:when>
      <xsl:when test="$sequence = 'first'">
      </xsl:when>
      <xsl:when test="$sequence = 'blank'">
      </xsl:when>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
