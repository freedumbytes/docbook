<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">

  <xsl:template match="d:cover" mode="titlepage.mode">
    <fo:block text-align="center">
      <xsl:apply-templates mode="titlepage.mode" />
    </fo:block>
  </xsl:template>

  <xsl:template match="d:copyright" mode="titlepage.mode">
    <fo:block text-align="center" space-before="8pt">
      <xsl:call-template name="gentext">
        <xsl:with-param name="key" select="'Copyright'"/>
      </xsl:call-template>
      <xsl:call-template name="gentext.space"/>
      <xsl:call-template name="dingbat">
        <xsl:with-param name="dingbat">copyright</xsl:with-param>
      </xsl:call-template>
      <xsl:call-template name="gentext.space"/>
      <xsl:call-template name="copyright.years">
        <xsl:with-param name="years" select="d:year"/>
        <xsl:with-param name="print.ranges" select="$make.year.ranges"/>
        <xsl:with-param name="single.year.ranges"
                        select="$make.single.year.ranges"/>
      </xsl:call-template>
      <xsl:call-template name="gentext.space"/>
      <xsl:apply-templates select="d:holder" mode="titlepage.mode"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="d:edition" mode="titlepage.mode">
    <fo:block text-align="center" font-weight="bold" space-before="8pt">
      <xsl:apply-templates mode="titlepage.mode"/>
      <xsl:call-template name="gentext.space"/>
      <xsl:if test="$editedby.enabled = 1">
        <xsl:call-template name="gentext">
          <xsl:with-param name="key" select="'Edition'"/>
        </xsl:call-template>
      </xsl:if>
    </fo:block>
  </xsl:template>

  <xsl:template match="d:pubdate" mode="titlepage.mode">
    <fo:block text-align="center" font-weight="bold" space-before="8pt">
      <xsl:if test="$publishedby.enabled = 1">
        <xsl:call-template name="gentext">
          <xsl:with-param name="key" select="'Published'" />
        </xsl:call-template>
      </xsl:if>
      <xsl:call-template name="gentext.space" />
      <xsl:apply-templates mode="titlepage.mode" />
    </fo:block>
  </xsl:template>

  <xsl:template match="d:corpauthor" mode="titlepage.mode">
    <fo:block text-align="center" font-weight="bold" space-before="8pt">
      <xsl:apply-templates mode="titlepage.mode"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="d:org" mode="titlepage.mode">
    <fo:block text-align="center" space-before="8pt">
      <xsl:apply-templates mode="titlepage.mode"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="d:orgname" mode="titlepage.mode">
    <fo:block text-align="center" font-weight="bold" space-before="8pt">
      <xsl:apply-templates mode="titlepage.mode"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="d:address" mode="titlepage.mode">
    <fo:block text-align="center" space-before="8pt">
      <xsl:if test="d:street">
        <fo:block>
          <xsl:apply-templates mode="titlepage.mode" select="d:street" />
        </fo:block>
      </xsl:if>
      <xsl:if test="d:otheraddr">
        <fo:block>
          <xsl:apply-templates mode="titlepage.mode" select="d:otheraddr" />
        </fo:block>
      </xsl:if>
      <xsl:if test="d:city or d:state or d:postcode">
        <fo:block>
          <xsl:apply-templates mode="titlepage.mode" select="d:city" />
          <xsl:if test="d:city and (d:state or d:postcode)">
            <xsl:text>, </xsl:text>
          </xsl:if>
          <xsl:apply-templates mode="titlepage.mode" select="d:state" />
          <xsl:if test="d:state">
            <xsl:text> </xsl:text>
          </xsl:if>
          <xsl:apply-templates mode="titlepage.mode" select="d:postcode" />
        </fo:block>
      </xsl:if>
      <xsl:if test="d:pob">
        <fo:block>
          <xsl:apply-templates mode="titlepage.mode" select="d:pob" />
        </fo:block>
      </xsl:if>
      <xsl:if test="d:country">
        <fo:block>
          <xsl:apply-templates mode="titlepage.mode" select="d:country" />
        </fo:block>
      </xsl:if>
      <xsl:if test="d:phone">
        <fo:block>
          <xsl:apply-templates mode="titlepage.mode" select="d:phone" />
        </fo:block>
      </xsl:if>
      <xsl:if test="d:fax">
        <fo:block>
          <xsl:apply-templates mode="titlepage.mode" select="d:fax" />
        </fo:block>
      </xsl:if>
      <xsl:if test="d:email">
        <fo:block>
          <xsl:apply-templates mode="titlepage.mode" select="d:email" />
        </fo:block>
      </xsl:if>
    </fo:block>
  </xsl:template>

  <xsl:template match="d:author|d:editor|d:othercredit|d:publisher|d:collab" mode="titlepage.mode">
    <xsl:call-template name="credits.div"/>
  </xsl:template>

  <xsl:template match="d:contrib" mode="titlepage.mode">
    <fo:block text-align="center" space-before="8pt" space-after="6pt">
      <xsl:apply-templates mode="titlepage.mode"/>
    </fo:block>
  </xsl:template>

  <xsl:template name="person.name.first-last">
    <xsl:param name="node" select="."/>

    <xsl:if test="$node//d:honorific">
      <xsl:apply-templates select="$node//d:honorific[1]"/>
      <xsl:value-of select="$punct.honorific"/>
    </xsl:if>

    <xsl:if test="$node//d:firstname">
      <xsl:if test="$node//d:honorific">
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="$node//d:firstname[1]"/>
    </xsl:if>

    <xsl:if test="$node/d:othername and $author.othername.in.middle != 0">
      <xsl:if test="$node//d:honorific or $node//d:firstname">
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="$node/d:othername[1]"/>
    </xsl:if>

    <xsl:if test="$node//d:surname">
      <xsl:if test="$node//d:honorific or $node//d:firstname
                    or ($node/d:othername and $author.othername.in.middle != 0)">
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="$node//d:surname[1]"/>
    </xsl:if>

    <xsl:if test="$node//d:lineage">
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="$node//d:lineage[1]"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="credits.div">
    <fo:block text-align="center" space-before="8pt">
      <xsl:if test="self::d:editor[position()=1] and not($editedby.enabled = 0)">
        <fo:block text-align="center" font-weight="bold" space-after="6pt">
          <xsl:call-template name="gentext.edited.by" />
        </fo:block>
      </xsl:if>
      <xsl:if test="self::d:publisher and not($publishedby.enabled = 0)">
        <fo:block text-align="center" font-weight="bold" space-after="6pt">
          <xsl:call-template name="gentext.published.by" />
        </fo:block>
      </xsl:if>
      <fo:block font-weight="bold">
        <xsl:if test="d:orgname">
          <xsl:apply-templates select="d:orgname" />
        </xsl:if>
        <xsl:if test="d:publishername">
          <xsl:apply-templates select="d:publishername" mode="titlepage.mode" />
        </xsl:if>
        <xsl:if test="d:collabname">
          <xsl:apply-templates select="d:collabname" mode="titlepage.mode" />
          <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:call-template name="person.name" />
      </fo:block>
      <xsl:apply-templates mode="titlepage.mode" select="d:address" />
      <xsl:apply-templates mode="titlepage.mode" select="d:affiliation" />
      <xsl:apply-templates mode="titlepage.mode" select="d:email|d:person/d:email" />
      <xsl:if test="not($blurb.on.titlepage.enabled = 0)">
        <xsl:apply-templates mode="titlepage.mode" select="d:contrib|d:authorblurb|d:personblurb|d:person/d:personblurb" />
      </xsl:if>
    </fo:block>
  </xsl:template>

  <xsl:template match="d:authorblurb" mode="titlepage.mode">
    <fo:block space-after="8pt">
      <xsl:apply-templates mode="titlepage.mode"/>
    </fo:block>
  </xsl:template>

  <xsl:template match="d:personblurb" mode="titlepage.mode">
    <fo:block space-after="8pt">
      <xsl:apply-templates mode="titlepage.mode"/>
    </fo:block>
  </xsl:template>
</xsl:stylesheet>
