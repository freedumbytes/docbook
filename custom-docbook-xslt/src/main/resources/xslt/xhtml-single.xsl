<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">
  <xsl:import href="http://docbook.sourceforge.net/release/xsl/1.78.1/xhtml5/docbook.xsl" />

  <xsl:include href="common/en.xml" />

  <xsl:include href="xhtml/param.xsl" />

  <xsl:include href="xhtml/admon.xsl" />
  <xsl:include href="xhtml/callout.xsl" />
  <xsl:include href="xhtml/docbook.xsl" />
  <xsl:include href="xhtml/html.xsl" />
  <xsl:include href="xhtml/inline.xsl" />
  <xsl:include href="xhtml/programlisting.xsl" />
  <xsl:include href="xhtml/table.xsl" />
  <xsl:include href="xhtml/titlepage.templates.xsl" />
  <xsl:include href="xhtml/titlepage.xsl" />
</xsl:stylesheet>
