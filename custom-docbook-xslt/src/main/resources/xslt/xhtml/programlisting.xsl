<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">

  <!-- <programlisting/> highlighting using highlight.js (allows decent copy-pasting by end-users) -->

  <xsl:template match="d:programlisting">
    <xsl:variable name="language">
      <xsl:value-of select="s:toLowerCase(string(@language))" xmlns:s="java:java.lang.String" />
    </xsl:variable>
    <xsl:choose>
      <xsl:when test="$language != ''">
        <pre class="language-provided">
          <code class="language-{$language}">
            <xsl:apply-templates />
          </code>
        </pre>
      </xsl:when>
      <xsl:otherwise>
        <pre>
          <code class="no-highlight">
            <xsl:apply-templates />
          </code>
        </pre>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
