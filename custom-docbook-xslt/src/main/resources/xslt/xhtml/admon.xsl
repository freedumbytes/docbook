<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">
  <!--
    From: xhtml/admon.xsl
    Reason: remove tables
  -->
  <xsl:template name="graphical.admonition">
    <xsl:variable name="admon.type">
      <xsl:choose>
        <xsl:when test="local-name(.)='note'">Note</xsl:when>
        <xsl:when test="local-name(.)='warning'">Warning</xsl:when>
        <xsl:when test="local-name(.)='caution'">Caution</xsl:when>
        <xsl:when test="local-name(.)='tip'">Tip</xsl:when>
        <xsl:when test="local-name(.)='important'">Important</xsl:when>
        <xsl:otherwise>Note</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>

    <xsl:variable name="alt">
      <xsl:call-template name="gentext">
        <xsl:with-param name="key" select="$admon.type" />
      </xsl:call-template>
    </xsl:variable>

    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="class.attribute" />
      <xsl:if test="$admon.style != ''">
        <xsl:attribute name="style">
          <xsl:value-of select="$admon.style" />
        </xsl:attribute>
      </xsl:if>

      <xsl:call-template name="anchor" />
      <xsl:if test="$admon.textlabel != 0 or d:title">
        <h2><xsl:apply-templates select="." mode="object.title.markup" /></h2>
      </xsl:if>
      <xsl:apply-templates />
    </div>
  </xsl:template>
</xsl:stylesheet>
