<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">

  <xsl:template match="d:cover" mode="titlepage.mode">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes"/>
      <xsl:apply-templates mode="titlepage.mode" />
    </div>
  </xsl:template>

  <xsl:template match="d:edition" mode="titlepage.mode">
    <h3 xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes"/>
      <xsl:apply-templates mode="titlepage.mode" />
      <xsl:call-template name="gentext.space" />
      <xsl:if test="$editedby.enabled = 1">
        <xsl:call-template name="gentext">
          <xsl:with-param name="key" select="'Edition'" />
        </xsl:call-template>
      </xsl:if>
    </h3>
  </xsl:template>

  <xsl:template match="d:pubdate" mode="titlepage.mode">
    <h4 xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes"/>
      <xsl:if test="$publishedby.enabled = 1">
        <xsl:call-template name="gentext">
          <xsl:with-param name="key" select="'Published'" />
        </xsl:call-template>
      </xsl:if>
      <xsl:call-template name="gentext.space" />
      <xsl:apply-templates mode="titlepage.mode" />
    </h4>
  </xsl:template>

  <xsl:template match="d:corpauthor" mode="titlepage.mode">
    <h5 xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes"/>
      <xsl:call-template name="id.attribute"/>
      <xsl:apply-templates mode="titlepage.mode"/>
    </h5>
    <br />
  </xsl:template>

  <xsl:template match="d:org" mode="titlepage.mode">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes"/>
      <xsl:call-template name="id.attribute"/>
      <xsl:apply-templates mode="titlepage.mode"/>
    </div>
    <br />
  </xsl:template>

  <xsl:template match="d:orgname" mode="titlepage.mode">
    <h5 xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes"/>
      <xsl:call-template name="id.attribute"/>
      <xsl:apply-templates mode="titlepage.mode"/>
    </h5>
  </xsl:template>

  <xsl:template match="d:address" mode="titlepage.mode">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes"/>
      <xsl:call-template name="id.attribute"/>
      <xsl:if test="d:street">
        <div>
          <xsl:apply-templates mode="titlepage.mode" select="d:street" />
        </div>
      </xsl:if>
      <xsl:if test="d:otheraddr">
        <div>
          <xsl:apply-templates mode="titlepage.mode" select="d:otheraddr" />
        </div>
      </xsl:if>
      <xsl:if test="d:city or d:state or d:postcode">
        <div>
          <xsl:apply-templates mode="titlepage.mode" select="d:city" />
          <xsl:if test="d:city and (d:state or d:postcode)">
            <xsl:text>, </xsl:text>
          </xsl:if>
          <xsl:apply-templates mode="titlepage.mode" select="d:state" />
          <xsl:if test="d:state">
            <xsl:text> </xsl:text>
          </xsl:if>
          <xsl:apply-templates mode="titlepage.mode" select="d:postcode" />
        </div>
      </xsl:if>
      <xsl:if test="d:pob">
        <div>
          <xsl:apply-templates mode="titlepage.mode" select="d:pob" />
        </div>
      </xsl:if>
      <xsl:if test="d:country">
        <div>
          <xsl:apply-templates mode="titlepage.mode" select="d:country" />
        </div>
      </xsl:if>
      <xsl:if test="d:phone">
        <div>
          <xsl:apply-templates mode="titlepage.mode" select="d:phone" />
        </div>
      </xsl:if>
      <xsl:if test="d:fax">
        <div>
          <xsl:apply-templates mode="titlepage.mode" select="d:fax" />
        </div>
      </xsl:if>
      <xsl:if test="d:email">
        <div>
          <xsl:apply-templates mode="titlepage.mode" select="d:email" />
        </div>
      </xsl:if>
    </div>
  </xsl:template>

  <xsl:template match="d:author|d:editor|d:othercredit|d:publisher|d:collab" mode="titlepage.mode">
    <xsl:call-template name="credits.div"/>
  </xsl:template>

  <xsl:template match="d:contrib" mode="titlepage.mode">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes"/>
      <xsl:call-template name="id.attribute"/>
      <p><xsl:apply-templates mode="titlepage.mode"/></p>
    </div>
  </xsl:template>

  <xsl:template name="person.name.first-last">
    <xsl:param name="node" select="."/>

    <xsl:if test="$node//d:honorific">
      <xsl:apply-templates select="$node//d:honorific[1]"/>
      <xsl:value-of select="$punct.honorific"/>
    </xsl:if>

    <xsl:if test="$node//d:firstname">
      <xsl:if test="$node//d:honorific">
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="$node//d:firstname[1]"/>
    </xsl:if>

    <xsl:if test="$node/d:othername and $author.othername.in.middle != 0">
      <xsl:if test="$node//d:honorific or $node//d:firstname">
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="$node/d:othername[1]"/>
    </xsl:if>

    <xsl:if test="$node//d:surname">
      <xsl:if test="$node//d:honorific or $node//d:firstname
                    or ($node/d:othername and $author.othername.in.middle != 0)">
        <xsl:text> </xsl:text>
      </xsl:if>
      <xsl:apply-templates select="$node//d:surname[1]"/>
    </xsl:if>

    <xsl:if test="$node//d:lineage">
      <xsl:text>, </xsl:text>
      <xsl:apply-templates select="$node//d:lineage[1]"/>
    </xsl:if>
  </xsl:template>

  <xsl:template name="credits.div">
    <div xmlns="http://www.w3.org/1999/xhtml">
      <xsl:apply-templates select="." mode="common.html.attributes" />
      <xsl:call-template name="id.attribute" />
      <xsl:if test="self::d:editor[position()=1] and not($editedby.enabled = 0)">
        <h6 class="editedby">
          <xsl:call-template name="gentext.edited.by" />
        </h6>
      </xsl:if>
      <xsl:if test="self::d:publisher and not($publishedby.enabled = 0)">
        <h6 class="publishedby">
          <xsl:call-template name="gentext.published.by" />
        </h6>
      </xsl:if>
      <h5>
        <xsl:apply-templates select="." mode="common.html.attributes" />
        <xsl:if test="d:orgname">
          <xsl:apply-templates select="d:orgname" />
        </xsl:if>
        <xsl:if test="d:publishername">
          <xsl:apply-templates select="d:publishername" mode="titlepage.mode" />
        </xsl:if>
        <xsl:if test="d:collabname">
          <xsl:apply-templates select="d:collabname" mode="titlepage.mode" />
          <xsl:text> </xsl:text>
        </xsl:if>
        <xsl:call-template name="person.name" />
      </h5>
      <xsl:apply-templates mode="titlepage.mode" select="d:address" />
      <xsl:apply-templates mode="titlepage.mode" select="d:affiliation" />
      <xsl:apply-templates mode="titlepage.mode" select="d:email|d:person/d:email" />
      <xsl:if test="not($blurb.on.titlepage.enabled = 0)">
        <xsl:apply-templates mode="titlepage.mode" select="d:contrib|d:authorblurb|d:personblurb|d:person/d:personblurb" />
      </xsl:if>
    </div>
    <br />
  </xsl:template>
</xsl:stylesheet>
