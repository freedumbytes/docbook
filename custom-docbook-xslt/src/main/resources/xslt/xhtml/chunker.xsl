<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">
  <xsl:param name="chunk.first.sections" select="1" />
  <xsl:param name="chunk.section.depth" select="0" />
  <xsl:param name="chunk.toc" select="''" />
  <xsl:param name="chunker.output.doctype-public" select="'-//W3C//DTD XHTML 1.0 Strict//EN'" />
  <xsl:param name="chunker.output.doctype-system" select="'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'" />
  <xsl:param name="chunker.output.encoding" select="'UTF-8'" />
</xsl:stylesheet>
