<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">

  <xsl:template name="book.titlepage.recto">
    <xsl:choose>
      <xsl:when test="d:bookinfo/d:title">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:title" />
      </xsl:when>
      <xsl:when test="d:info/d:title">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:title" />
      </xsl:when>
      <xsl:when test="d:title">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:title" />
      </xsl:when>
    </xsl:choose>

    <xsl:choose>
      <xsl:when test="d:bookinfo/d:subtitle">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:bookinfo/d:subtitle" />
      </xsl:when>
      <xsl:when test="d:info/d:subtitle">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:info/d:subtitle" />
      </xsl:when>
      <xsl:when test="d:subtitle">
        <xsl:apply-templates mode="book.titlepage.recto.auto.mode" select="d:subtitle" />
      </xsl:when>
    </xsl:choose>

    <xsl:if test="($confidential.mode = 'yes' or ($confidential.mode = 'maybe' and ancestor-or-self::*[@security][1]/@security = 'confidential'))">
      <h1 xmlns="http://www.w3.org/1999/xhtml" class="confidential">
        <xsl:text> </xsl:text>
      </h1>
    </xsl:if>

    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:cover" />

    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:corpauthor"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:corpauthor"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:org"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:org"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:authorgroup"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:authorgroup"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:author"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:author"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:othercredit"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:othercredit"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:collab"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:collab"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:copyright" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:copyright" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:publisher" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:publisher" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:pubdate" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:pubdate" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:edition" />
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:edition" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:editor"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:editor"/>
    <xsl:apply-templates mode="titlepage.mode" select="d:info/d:legalnotice" />
    <xsl:apply-templates mode="titlepage.mode" select="d:bookinfo/d:legalnotice" />
  </xsl:template>
</xsl:stylesheet>
