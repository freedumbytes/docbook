<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">
  <!--
     From: xhtml/docbook.xsl
     Reason: to apply a charset <meta/> tag to the xhtml <head/>.
  -->
  <xsl:template name="user.head.content">
    <xsl:param name="node" select="." />
    <meta xmlns="http://www.w3.org/1999/xhtml" http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link xmlns="http://www.w3.org/1999/xhtml" rel="shortcut icon" href="./images/community/docbook/glider.ico" />
  </xsl:template>

  <!--
    From: xhtml/docbook.xsl
    Reason: Add css class for draft mode
    Version: 1.72.0
  -->
  <xsl:template name="body.attributes">
    <xsl:if test="($draft.mode = 'yes' or ($draft.mode = 'maybe' and ancestor-or-self::*[@status][1]/@status = 'draft'))">
      <xsl:attribute name="class">
        <xsl:value-of select="ancestor-or-self::*[@status][1]/@status" />
      </xsl:attribute>
    </xsl:if>
  </xsl:template>

  <xsl:template name="user.header.content">
    <xsl:param name="node" select="." />
    <xsl:if test="($confidential.mode = 'yes' or ($confidential.mode = 'maybe' and ancestor-or-self::*[@security][1]/@security = 'confidential'))">
      <h1 xmlns="http://www.w3.org/1999/xhtml" class="confidential">
        <xsl:text> </xsl:text>
      </h1>
    </xsl:if>
  </xsl:template>

  <xsl:template name="user.footer.content">
    <script type="text/javascript" src="js/highlight.min.js">
      <!-- Workaround to force outputting "</script>". The space is required. -->
      <xsl:text> </xsl:text>
    </script>
    <script type="text/javascript">
      <xsl:text>hljs.highlightAll();</xsl:text>
    </script>
  </xsl:template>
</xsl:stylesheet>
