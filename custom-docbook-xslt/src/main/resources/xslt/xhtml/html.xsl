<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">
  <xsl:template name="anchor">
    <xsl:param name="node" select="." />
    <xsl:param name="conditional" select="1" />
    <xsl:variable name="id">
      <xsl:call-template name="object.id">
        <xsl:with-param name="object" select="$node" />
      </xsl:call-template>
    </xsl:variable>

    <xsl:if test="$conditional = 0 or $node/@id or $node/@xml:id">
      <xsl:choose>
        <xsl:when test="local-name($node) = 'preface' or local-name($node) = 'chapter' or local-name($node) = 'section' or local-name($node) = 'appendix'">
          <a xmlns="http://www.w3.org/1999/xhtml" id="{$id}" class="anchor" href="#{$id}">
            <span class="octicon octicon-link"></span>
          </a>
        </xsl:when>
        <xsl:otherwise>
          <a xmlns="http://www.w3.org/1999/xhtml" id="{$id}">
            <span style="display:none;">trick</span>
          </a>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:if>
  </xsl:template>

  <xsl:template name="generate.html.title" />
</xsl:stylesheet>
