<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                exclude-result-prefixes="d">
  <xsl:output method="xml"
              encoding="UTF-8"
              standalone="no"
              doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
              doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" />

  <xsl:param name="admon.graphics" select="1" />
  <xsl:param name="admon.graphics.path">
    <xsl:if test="$img.src.path != ''">
      <xsl:value-of select="$img.src.path" />
    </xsl:if>
    <xsl:text>images/community/docbook/admonitions/</xsl:text>
  </xsl:param>

  <xsl:param name="callouts.extension">1</xsl:param>
  <xsl:param name="callout.defaultcolumn">15</xsl:param>
  <xsl:param name="callout.graphics.extension">.png</xsl:param>
  <xsl:param name="callout.graphics" select="1"/>
  <xsl:param name="callout.icon.size">17px</xsl:param>
  <xsl:param name="callout.graphics.number.limit">15</xsl:param>
  <xsl:param name="callout.graphics.path">
    <xsl:if test="$img.src.path != ''">
      <xsl:value-of select="$img.src.path"/>
    </xsl:if>
    <xsl:text>images/community/docbook/callouts/</xsl:text>
  </xsl:param>

  <xsl:param name="confidential.mode">maybe</xsl:param>

  <xsl:param name="css.decoration" select="0" />

  <xsl:param name="draft.mode">maybe</xsl:param>
  <xsl:param name="draft.watermark.image">images/community/docbook/status/draft.png</xsl:param>

  <xsl:param name="editedby.enabled" select="1" />
  <xsl:param name="publishedby.enabled" select="1" />
  <xsl:param name="blurb.on.titlepage.enabled" select="1" />

  <xsl:param name="footer.rule" select="0" />

  <xsl:param name="formal.title.placement">
    figure after
    example before
    equation before
    table before
    procedure before
  </xsl:param>

  <xsl:param name="generate.legalnotice.link" select="0" />
  <xsl:param name="generate.revhistory.link" select="0" />

  <xsl:param name="generate.toc">
    set toc
    book toc,title
    article nop
    chapter toc,title
    appendix toc,title
    qandadiv toc
    qandaset toc
    sect1 nop
    sect2 nop
    sect3 nop
    sect4 nop
    sect5 nop
    section toc
    part toc
  </xsl:param>

  <xsl:param name="graphicsize.extension">0</xsl:param>

  <xsl:param name="header.rule" select="0" />

  <xsl:param name="html.cleanup" select="1" />
  <xsl:param name="html.ext" select="'.html'" />
  <xsl:param name="html.stylesheet" select="'css/freedumbytes.css'" />
  <xsl:param name="html.stylesheet.type" select="'text/css'" />

  <xsl:param name="ignore.image.scaling" select="1" />

  <!-- Enable line numbering extension. I have personally not gotten this to work yet -->
  <xsl:param name="linenumbering.extension">1</xsl:param>
  <xsl:param name="linenumbering.width">2</xsl:param>
  <xsl:param name="linenumbering.everyNth">1</xsl:param>

  <xsl:param name="menuchoice.menu.separator"> &#8594; </xsl:param>
  <xsl:param name="menuchoice.separator"> &#8594; </xsl:param>

  <xsl:param name="section.autolabel" select="1" />
  <xsl:param name="section.label.includes.component.label" select="1" />

  <xsl:param name="suppress.navigation" select="0" />
  <xsl:param name="suppress.footer.navigation" select="0" />
  <xsl:param name="suppress.header.navigation" select="0" />

  <xsl:param name="table.cell.border.style" />

  <xsl:param name="ulink.target" />

  <xsl:param name="use.extensions">1</xsl:param>
  <xsl:param name="use.id.as.filename" select="1"/>
</xsl:stylesheet>
