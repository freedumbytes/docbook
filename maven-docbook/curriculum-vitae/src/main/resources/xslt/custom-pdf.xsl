<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:d="http://docbook.org/ns/docbook"
                xmlns:jbh="java:org.jboss.highlight.renderer.FORenderer"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                exclude-result-prefixes="d jbh">
  <xsl:import href="classpath:xslt/pdf.xsl" />

  <xsl:include href="fo/old-font.xsl" />

  <xsl:param name="confidential.text">Vertrouwelijk</xsl:param>
</xsl:stylesheet>
