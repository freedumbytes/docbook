# DocBook Tools

## DocBook

[DocBook](https://docbook.org) is, in part, _an XML vocabulary that lets you create documents in a presentation-neutral form that captures the logical structure of your content_.
Another aspect of DocBook is the rendering of that content into various formats using DocBook-supplied (or custom) XSLT stylesheets.
Basically, DocBook allows you to write and maintain a single source for documentation, and to then render that single source into multiple formats such as PDF or HTML.

[The DocBook Project](http://docbook.sourceforge.net) supports the open-source development of a variety of DocBook resources; in particular, [the DocBook XSL stylesheets](https://cdn.docbook.org).

[PressGang](https://pressgang.jboss.org) is the [home](https://github.com/pressgang) of PressGang CCMS and the JBoss Base Styles. We are here to help you write awesome user documentation that meets the needs of your users.

The purpose of [the PressGang Maven jDocBook Plugin](https://maven-jdocbook-plugin.jboss.org) is to allow these DocBook transformations to occur as a natural part of the users Maven build.
The main difficulty with this has always been the fact that DocBook transformations are usually very closely tied to the user's local environment.
The design goal with writing this plugin was to utilize Maven's dependency mechanism to bring all the pieces together on demand. Those pieces are:

 * [PressGang Maven jDocBook Plugin](https://github.com/pressgang/maven-jdocbook-plugin) adds support for DocBook handling to Maven.

 * [PressGang Maven Docbook Style Plugin](https://github.com/pressgang/maven-jdocbook-style-plugin) defines the custom _jdocbook-style_ packaging.

 * [Pressgang Tools](https://github.com/pressgang/pressgang-tools) for all artifacts which support JBoss Community documentation:

   * [JBoss Community Documentation Style](https://github.com/pressgang/pressgang-tools/tree/master/pressgang-jdocbook-style) for Blue and Grey CSS and Images to use as a base for your project XHTML JBoss Community Documentation.

   * [JBoss Community DocBook XSLT](https://github.com/pressgang/pressgang-tools/tree/master/pressgang-xslt-ns) is the main project to transform DocBook XML into XHTML and other formats for JBoss Community Documentation.
     Use this with XSD based docbook XML files.

   * [JBoss Community DocBook Code Highlighter](https://github.com/pressgang/pressgang-tools/tree/master/pressgang-highlight) provides code highlighting for documentation examples. Currently uses the jHighlight library.

   * [JBoss.org PDF Fonts](https://github.com/pressgang/pressgang-tools/tree/master/pressgang-fonts) contains both the fonts themselves and the XSLT templates necessary to get them included into the generated PDFs
     (simply import/include this XSLT into your project's PDF XSLT).

## XML Graphics FOP

The Apache XML Graphics Project is responsible for the conversion of XML formats to graphical output:

 * [Apache XML Graphics Commons](https://xmlgraphics.apache.org/commons) is a library that consists of several reusable components used by Apache Batik and Apache FOP.
   Many of these components can easily be used separately outside the domains of SVG and XSL-FO.
   You will find components such as a PDF library, an RTF library, Graphics2D implementations that let you generate PDF & PostScript files, and much more.

 * [Apache Batik](https://xmlgraphics.apache.org/batik) is a Java-based toolkit for applications or applets that want to use images in the Scalable Vector Graphics (SVG) format for various purposes, such as display, generation or manipulation.

 * [Apache FOP](https://xmlgraphics.apache.org/fop) (Formatting Objects Processor) is a print formatter driven by XSL formatting objects (XSL-FO) and an output independent formatter.
   It is a Java application that reads a formatting object (FO) tree and renders the resulting pages to a specified output.
   Output formats currently supported include PDF, PS, PCL, AFP, XML (area tree representation), Print, AWT and PNG, and to a lesser extent, RTF and TXT.
   The primary output target is PDF.

   * [Apache FOP Hyphenation](https://xmlgraphics.apache.org/fop/trunk/hyphenation.html) uses Liang's hyphenation algorithm, well known from [TeX](https://tug.org) (τέχνη).
     It needs language specific pattern and other data for operation.

**Note**: Because of licensing issues (and for convenience), all hyphenation patterns for FOP are made available through the [OFFO](http://offo.sourceforge.net) (Objects For Formatting Objects) project.

## Font

In [metal](https://en.wikipedia.org/wiki/Movable_type) [typesetting](https://en.wikipedia.org/wiki/Typesetting),
a **[font](https://en.wikipedia.org/wiki/Font)** is a particular size, weight and style of a [typeface](https://en.wikipedia.org/wiki/Typeface).
Each font is a matched set of type, with a piece (a [sort](https://en.wikipedia.org/wiki/Sort_(typesetting))) for each [glyph](https://en.wikipedia.org/wiki/Glyph), and a typeface consisting of a range of fonts that shared an overall design.

In modern usage, with the advent of [computer fonts](https://en.wikipedia.org/wiki/Computer_font), the term _font_ has come to be used as a synonym for _typeface_ although a typical typeface (or _font family_) consists of a number of fonts.
For instance **[Roman](https://en.wikipedia.org/wiki/Roman_type)** (also **Regular**), **[Bold](https://en.wikipedia.org/wiki/Emphasis_(typography))** and **[Italic](https://en.wikipedia.org/wiki/Italic_type)**;
each of these exists in a variety of sizes.
The term _font_ is correctly applied to any one of these alone but may be seen used loosely to refer to the whole _typeface_. When used in computers, each style is in a separate digital _font file_.

In both traditional typesetting and modern usage, the word _font_ refers to the delivery mechanism of the typeface.
In traditional typesetting, the font would be made from metal or wood: to compose a page may require multiple fonts or even multiple typefaces.

### Complex Scripts

[Complex text layout](https://en.wikipedia.org/wiki/Complex_text_layout) (CTL) or complex text rendering is the [typesetting](https://en.wikipedia.org/wiki/Typesetting) of [writing systems](https://en.wikipedia.org/wiki/Writing_system)
in which the shape or positioning of a [grapheme](https://en.wikipedia.org/wiki/Grapheme) depends on its relation to other graphemes.
The term is used in the field of software [internationalization](https://en.wikipedia.org/wiki/Internationalization), where each grapheme is a [character](https://en.wikipedia.org/wiki/Character_(computing)).

Scripts which require CTL for proper display may be known as **complex scripts**. Examples include the [Arabic alphabet](https://en.wikipedia.org/wiki/Arabic_alphabet) and scripts of the [Brahmic family](https://en.wikipedia.org/wiki/Brahmic_scripts),
such as [Devanagari](https://en.wikipedia.org/wiki/Devanagari), [Khmer script](https://en.wikipedia.org/wiki/Khmer_script) or the [Thai alphabet](https://en.wikipedia.org/wiki/Thai_alphabet). Many scripts do not require CTL.
For instance, the [Latin alphabet](https://en.wikipedia.org/wiki/Latin_alphabet) or [Chinese characters](https://en.wikipedia.org/wiki/Chinese_character) can be typeset by simply displaying each character one after another in straight rows or columns.
However, even these scripts have alternate forms or optional features (such as [cursive](https://en.wikipedia.org/wiki/Cursive) writing) which require CTL to produce on computers.

### ISO 639 Codes

[ISO](https://en.wikipedia.org/wiki/International_Organization_for_Standardization) [639](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) is a standardized nomenclature used to classify languages.
Each language is assigned a two-letter (639-1) and three-letter (639-2 and 639-3) lowercase abbreviation, amended in later versions of the nomenclature.

### Windows

An important development in Windows 10 is the Universal Windows Platform (UWP): [Windows Fonts](https://docs.microsoft.com/en-us/typography/fonts/windows_10_font_list) are one aspect of this convergence.

A number of additional fonts are available for Desktop and Server, including all other fonts from previous releases. However, not all of these are pre-installed by default in all images.
Any of these Feature On Demand (FOD) packages can be installed manually. Press **Windows + I** on the keyboard to access **Settings** > **Apps** > **Apps & features** > **Optional features** > **Add a feature** and one of the _Supplemental Fonts_.

How to [Download and Install Fonts](https://adamtheautomator.com/install-fonts-windows-10) in Windows 10.

### Google

[Noto](https://fonts.google.com/noto) is a collection of high-quality fonts with multiple weights and widths in sans, serif, mono, and other styles.
The [Noto Fonts](https://github.com/googlefonts/noto-fonts) are perfect for harmonious, aesthetic, and typographically correct global communication, in more than 1,000 languages and over 150 writing systems.

_Noto_ means _I write, I mark, I note_ in Latin. The name is also short for _no tofu_, as the project aims to eliminate **tofu**: blank rectangles shown when no font is available for your text.

If you want to use Noto on Windows, please [download hinted fonts](https://github.com/googlefonts/noto-fonts/tree/main/hinted/ttf).
Select all the `.ttf` and **right-click** > **Install for all users** to the `%SystemRoot%\Fonts` location and this registers the fonts so that they are available to all Windows applications.

### Ubuntu

The [Ubuntu](https://design.ubuntu.com/font) typeface has been specially created to complement the Ubuntu tone of voice.
It has a contemporary style and contains characteristics unique to the Ubuntu brand that convey a precise, reliable and free attitude.

### Java Sample Font Check

Method `canDisplayUpTo` indicates whether or not this `Font` can display a specified `String`. For strings with Unicode encoding, it is important to know if a particular font can display the string.
This method returns an offset into the string which is the first character this `Font` cannot display without using the missing glyph code. If the `Font` can display all characters, -1 is returned.

```java
  public static void main(String[] args) {
    Font[] systemFonts = GraphicsEnvironment.getLocalGraphicsEnvironment().getAllFonts();

    for (Font systemFont : systemFonts)
    {
      if ((systemFont.canDisplayUpTo("𑌸𑌂𑌸𑍍𑌕𑍃𑌤𑌮𑍍") < 0) || (systemFont.canDisplayUpTo("संस्कृतम्") < 0))
      {
        System.err.println(systemFont);
      }
      else
      {
        System.out.println(systemFont);
      }
    }
  }
```

## Maven

### Patches

#### PressGang jDocBook Core

 * FopConfigHelper

   * Upgrade FOP 1.1 - NoSuchMethodError: `org.apache.fop.fonts.FontSetup.createMinimalFontResolver()Lorg/apache/fop/fonts/FontResolver`.
   * Font Metrics XML support - An option to change possible wrong `ascender` and `descender` metrics for some Noto Sans fonts
     (see also [![issue FOP-2657](https://img.shields.io/jira/issue/https/issues.apache.org/jira/FOP-2657.svg "issue FOP-2657")](https://issues.apache.org/jira/browse/FOP-2657)).
   * Use correct attribute values of `name`, `style` and `weight` in `font-triplet` because for some fonts `emphasis` with `role="bold"` was not working, when more than one `.ttf` file is required
     (for example with `NotoSansJavanese-Regular.ttf` and `NotoSansSundanese-Bold.ttf`).
   * Upgrade FOP 2.6 - UnsupportedOperationException: `coverage set class table not yet supported` thrown by `FontInfoFinder` for `NotoSansGrantha-Regular.ttf`.
     (see also [![issue FOP-2704](https://img.shields.io/jira/issue/https/issues.apache.org/jira/FOP-2704.svg "issue FOP-2704")](https://issues.apache.org/jira/browse/FOP-2704)).
   * Force reload of FopFactory in case of a different JDocBookComponentRegistry (for example in case of multi-modules or multiple executions with different configurations).

**Note**: Due to upgrade FOP 2.6 all dependencies for Batik are upgraded to 1.14, which causes the following error `SVG graphic could not be built` for `.svg` files, that simple link to a `.png` counterpart in the same folder:

```xml
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg>
   …
  <image
     y="0"
     x="0"
     id="image2579"
     height="48"
     width="48"
     sodipodi:absref="important.png"
     xlink:href="important.png" />
</svg>
```

With these specific error details:

```
SEVERE: SVG graphic could not be built.
Reason: org.apache.batik.bridge.BridgeException: file:/…/docbook/docbook/./:0
        The URI "file:/…/docbook/./important.png" on element <image> can't be opened because:
        The URI can't be opened: …\docbook\.\important.png (The system cannot find the file specified).
```

 * ResultImpl

   * Switch to FOP `LoggingEventListener` - Suppress the overkill of FOP `Font "…,normal,700" not found. Substituting with "…,normal,400".` `DEBUG` messages.
   * Of course also upgrade FOP 2.6.

#### XML Graphics FOP

 * FontInfo

   * Method `findAdjustWeight` didn't find Bold weight fonts.

**Note**: This module `custom-fop` was dropped after upgrading FOP 2.6 for PressGang jDocBook Core in `custom-jdocbook-core`.

#### PressGang Highlight

 * FORenderer

   * Ellipsis - `…` was displayed as `&`.

### Usage

To be able to use the `maven-jdocbook-plugin` it needs access to some artifacts in the JBoss reporsitory, for example:

```
[INFO] Pressgang Tools Example (en-US) .................... FAILURE [  1.828 s]
       Failed to execute goal org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:resources (default-resources) on project pressgang-tools-example:
       Unexpected problem: Unable to locate artifact [net.socialchange.doctype:doctype-changer:jar:1.1]:
       Failure to find net.socialchange.doctype:doctype-changer:jar:1.1 in https://repo.maven.apache.org/maven2 was cached in the local repository
```

Hence the profile `jbossRepo` which is activated by default, when the property `nexusRepo` isn't supplied, to prevent the same issue in Eclipse.
In case all artifacts should come from a self hosted Nexus repository, disable this profile from the command line with `-DnexusRepo` or even better change the `apache-maven-3.x.y/conf/settings.xml` to something like this:

```xml
<settings>

   …

  <mirrors>
    <mirror>
      <id>nexus-central</id>
      <mirrorOf>*</mirrorOf>
      <name>Nexus Central</name>
      <url>http://nexus.dev.net/nexus/repository/maven-public</url>
    </mirror>
  </mirrors>

  <profiles>
    <profile>
      <id>development</id>
      <repositories>
        <repository>
          <id>central.dev.net</id>
          <url>http://nexus.dev.net/nexus/repository/maven-public</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
          </snapshots>
        </repository>
      </repositories>
      <pluginRepositories>
        <pluginRepository>
          <id>central.dev.net</id>
          <url>http://nexus.dev.net/nexus/repository/maven-public</url>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>
            <updatePolicy>always</updatePolicy>
          </snapshots>
        </pluginRepository>
      </pluginRepositories>
    </profile>
  </profiles>

  <activeProfiles>
    <activeProfile>development</activeProfile>
  </activeProfiles>
</settings>
```

Other profiles are:

 * `exampleModuleDocBook` - optionally activate `pressgang-tools-example` module because of release build issue when `custom-docbook-style` and `custom-docbook-xslt` aren't available yet.

 * `pressgangDocBookStyle` - default style when `customDocBookStyle` isn't activated.

 * `customDocBookStyle` - activate custom HTML and PDF styling and XSLTs.

 * `docbookHtmlOnly` - generate only chunked HTML.

 * `docbookPdfOnly` - generate only PDF.

 * `docbookHtmlSingleOnly` - generate only single HTML.

Thus generate the default PressGang example chunked HTML only with:

```
mvn install -DdocbookHtmlOnly -DexampleModuleDocBook
```

And generate the custom example PDF, single and chunked HTML with:

```
mvn install -DcustomDocBookStyle -DexampleModuleDocBook
```

When `custom-docbook-style` and `custom-docbook-xslt` modules aren't installed yet the following errors occur:

```
[ERROR] Some problems were encountered while processing the POMs:

[ERROR] Unresolveable build extension: Plugin org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9 or one of its dependencies could not be resolved:
        Could not find artifact nl.demon.shadowland.freedumbytes.documentation.docbook:custom-docbook-style:jdocbook-style:1.0.0-SNAPSHOT
         in jboss-public-repository-group (http://repository.jboss.org/nexus/content/groups/public/)

[ERROR] Unresolveable build extension: Plugin org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9 or one of its dependencies could not be resolved:
        Could not find artifact nl.demon.shadowland.freedumbytes.documentation.docbook:custom-docbook-xslt:jar:1.0.0-SNAPSHOT
         in jboss-public-repository-group (http://repository.jboss.org/nexus/content/groups/public/)
```

Therefore run `mvn install -DexampleModuleDocBook` first.

When there are changes in `custom-docbook-style`, ``, `` and `custom-docbook-xslt` modules the following errors might occur:

```
[ERROR] Failed to execute goal org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:generate (default-generate) on project pressgang-tools-example:
        Execution default-generate of goal org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:generate failed:

        A required class was missing while executing org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:generate: org/apache/fop/events/EventListener

        A required class was missing while executing org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:generate: org/jboss/jdocbook/profile/Profiler

        A required class was missing while executing org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:generate: org/jboss/jdocbook/render/Renderer

        A required class was missing while executing org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:generate: org/jboss/jdocbook/xslt/TransformerBuilder

        A required class was missing while executing org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:generate: org/jboss/jdocbook/render/XslFoGenerator
```

```
[ERROR] Failed to execute goal org.jboss.maven.plugins:maven-jdocbook-plugin:2.3.9:generate (default-generate) on project pressgang-tools-example:
        XSLT problem: error rendering [The URI java:org.jboss.highlight.renderer.FORenderer does not identify an external Java class;

        SystemID: jar:file:/…/custom-docbook-xslt/1.0.0-SNAPSHOT/custom-docbook-xslt-1.0.0-SNAPSHOT.jar!/xslt/fo/programlisting.xsl;
        Line#: 18; Column#: -1] on custom-master.xml -> [Help 1]

        SystemID: jar:file:/…/pressgang-xslt-ns-3.1.4.jar!/xslt/org/jboss/pressgang/pdf.xsl;
        Line#: 806; Column#: -1] on master.xml -> [Help 1]
```

In which case run `mvn install` without example module once or repeat the previous command because the changes should be installed by now.
